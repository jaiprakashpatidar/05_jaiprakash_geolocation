var map, infoWindow,service;
var start_pos;
/* Initialize the map with current location and  specify the markers on public bus stop with blue color & on hospitals with red color markers & on Movie theatres specify the sky blue color markers*/
function initMap() {
  var mapCenter = new google.maps.LatLng(12.9042502 ,77.60204569999999);
  start_pos=mapCenter
  console.log("start_pos");
  console.log(start_pos);
  map = new google.maps.Map(document.getElementById('map'), {
  												   center: mapCenter,
														 zoom: 17
  												 });
  service = new window.google.maps.places.PlacesService(map);
  infoWindow = new google.maps.InfoWindow;
  if (navigator.geolocation) {
    navigator.geolocation.getCurrentPosition(function(position) {
    	var pos = {
		    lat: position.coords.latitude,
		    lng: position.coords.longitude
    	};
    	mapCenter = new google.maps.LatLng(pos.lat ,pos.lng);
    	start_pos=pos;
   		console.log("start_pos");
    	console.log(mapCenter);
   		infoWindow.setPosition(pos);
  		//infoWindow.setContent('Location found.');
  		var marker = new google.maps.Marker({
    							   position: mapCenter,
    								 map: map,
										 title: 'Current Location'
  									});
		//	infoWindow.open(map);
    	map.setCenter(pos);
   	},
   	function() {
        handleLocationError(true, infoWindow, map.getCenter());
    });
  } else {
      // Browser doesn't support Geolocation
      handleLocationError(false, infoWindow, map.getCenter());
    }
  var searchTypes=['bus_station','movie_theater','hospital'];
  for(let i=0;i<searchTypes.length;i++) {
  	console.log(i)
  	var request = {
  	  location: mapCenter,
      radius: '500',
      type: searchTypes[i],
    };
  service.nearbySearch(request, callback);
  }
  
}

/* This function handles the condition if user reject the permission for accessing the current location detection */
function handleLocationError(browserHasGeolocation, infoWindow, pos) {
  infoWindow.setPosition(pos);
  infoWindow.setContent(browserHasGeolocation ?
                          'Error: The Geolocation service failed.' :
                          'Error: Your browser doesn\'t support geolocation.');
  infoWindow.open(map);
}

/*This call back function is called for every search like (bus_station,hospitals) and put the results in results variable and status is  varible which specify the placesService method  status*/
function callback(results, status) {
  console.log("length");
  console.log(results.length)
  if (status == google.maps.places.PlacesServiceStatus.OK) {
    for (var i = 0; i < results.length; i++) {
      var place = results[i];
      console.log(results[i]);
      console.log(place.geometry.location);
      createMarker(results[i]);
    }
  }
}

/* createMarker() is a function which create the marker and put the marker on particular location results by placesService() method */
function createMarker(pos) {
  var marker = new google.maps.Marker({
    					   position: pos.geometry.location,
								 title:pos.name,
		           });
  
  if(pos.types[0]==="bus_station")
    marker.setIcon("images/transport.png")
  if(pos.types[0]==="hospital")
	marker.setIcon("images/health-medical.png")	
  if(pos.types[0]==="movie_theater")
	marker.setIcon("images/movies.png")	
  // To add the marker to the map, call setMap();
  marker.setMap(map);

}
  